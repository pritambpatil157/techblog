<%-- 
    Document   : profile
    Created on : 31-Jul-2023, 8:49:24 pm
    Author     : Pritam
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tech.blog.entities.Category"%>
<%@page import="com.tech.blog.dao.PostDao"%>
<%@page import="com.tech.blog.helper.ConnectionProvider"%>
<%@page import="com.tech.blog.entities.Massage"%>
<%@page import="com.tech.blog.entities.User" %>
<%@page errorPage="error_page.jsp" %>
<% 

User user=(User)session.getAttribute("currentUser");

if(user==null){
 response.sendRedirect("login_page.jsp");

}
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
         <!--css-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"><!-- comment -->
        <link href="css/mycss.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!-- comment -->
        <style>
            .banner-background{
               clip-path: polygon(30% 0%, 70% 0%, 100% 0, 100% 92%, 73% 100%, 34% 94%, 0 100%, 0 0);
            }
        </style>
        
        
        
        
        
        
    </head>
    <body>
       <!--navbar-->
       <nav class="navbar navbar-expand-lg navbar-dark primary-background">
    <a class="navbar-brand" href="#"> <span class="fa fa-asterisk"></span>TechBlog</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
          <a class="nav-link" href="#"><span class="fa fa-bell-o"></span>LearnCode with Bloggers<span class="sr-only">(current)</span></a>
      </li>
     
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="fa fa-binoculars"></span> Categories
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Programing language</a>
          <a class="dropdown-item" href="#">Project implementation</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Data Structure</a>
        </div>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#"><span class="fa fa-fax"></span> Contant us</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#add-post-modal" ><span class="fa fa-asteric"></span> Do Post</a>
      </li>
     
       
<!--      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>-->
    </ul>
      <ul class="navbar-nav mr-right">
        <li class="nav-item">
            <a class="nav-link" href="#!" data-toggle="modal" data-target="#profile-Modal"><span class="fa fa-user-circle"></span><%= user.getName() %> </a>
        </li>  
        <li class="nav-item">
        <a class="nav-link" href="LogoutServlet"><span class="fa fa-user-plus"></span>Logout </a>
        </li>
      </ul>
  </div>
</nav>
       
       
   <!--end navbar-->
       <% 

                            Massage m=(Massage)session.getAttribute("msg");

                           if(m !=null){
                           
                                

                                %>
                            <div class="alert <%=m.getCssClass() %>" role="alert">
                                       <%= m.getContent() %>
                            </div>
                            <% 
                                session.removeAttribute("msg");
                                }
                            %>      
        
   
   <!--profileModel-->
   

<!-- Modal -->
<div class="modal fade" id="profile-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header primary-background text-white ">
        <h5 class="modal-title" id="exampleModalLabel">TechBlog</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="container text-center">
              <img src="pics/<%= user.getProfile() %>" class="img-fluid" style=" max-width:150px;" />
              <br>
              <h5 class="modal-title mt-3" id="exampleModalLabel"><%=user.getName() %></h5>
              <!--details-->
              <div id="profile_details">
              <table class="table">
                <tbody>
                        <tr>
                          <th scope="row">Id :</th>
                          <td><%= user.getId() %></td>

                        </tr>
                        <tr>
                          <th scope="row">Email :</th>
                          <td><%= user.getEmail() %></td>

                        </tr>
                        <tr>
                          <th scope="row">Gender :</th>
                          <td><%= user.getGender() %></td>

                        </tr>
                         <tr>
                          <th scope="row">Status :</th>
                          <td><%= user.getAbout() %></td>

                        </tr>
                        <tr>
                          <th scope="row">Register Date :</th>
                          <td><%= user.getTime().toString() %></td>

                        </tr>
                 </tbody>
               </table>
              </div>
                          <div id="profile_edit" style="display:none">
                              <h3 class="mt-2" >Please edit carefully</h3>
                              <form action="EditServlet" method="post" enctype="multipart/form-data">
                                  <table class="table">
                                        <tr>
                                            <td>ID :</td>
                                            <td><%= user.getId()%></td>
                                        </tr>
                                        <tr>
                                            <td>Email :</td>
                                            <td> <input type="email" class="form-control" name="user_email" value="<%= user.getEmail()%>" > </td>
                                        </tr> 
                                        <tr>
                                            <td>Name :</td>
                                            <td> <input type="text" class="form-control" name="user_name" value="<%= user.getName() %>" > </td>
                                        </tr> 
                                        <tr>
                                            <td>Password :</td>
                                            <td> <input type="password" class="form-control" name="user_password" value="<%= user.getPassword() %>" > </td>
                                        </tr> 
                                        <tr>
                                            <td>Gender :</td>
                                            <td> <%= user.getGender() %> </td>
                                        </tr> 
                                        <tr>
                                            <td>About :</td>
                                            <td> 
                                                <textarea class="form-control" name="user_about" rows="5">
                                                    <%= user.getAbout() %> 
                                                </textarea>
                                            
                                            
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td>New Profile:</td>
                                            <td>
                                                <input type="file" name="image" class="form-control" >
                                            </td>
                                        </tr>
                                  </table>
                                    <div class="container">
                                        <button type="submit" class="btn btn-outline-primary">Save</button>
                                    </div>
                              </form>
                          </div>
              
              
              
              
              
              
              
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button id="edit_profile_button" type="button" class="btn btn-primary">Edit</button>
      </div>
    </div>
  </div>
</div>
   
   
   
   
   
   
<!--endofModel-->
     
    
       
<!--add post Model-->
     
     

<!-- Modal -->
<div class="modal fade" id="add-post-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Provide the Post details..</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          
          <form id="add-post-form" action="AddPostServlet" method="post" >
              <div class="form-group"><!-- comment --> 
                  <select class="form-control" name="cId">
                     <option selected disabled>--Select Category--</option>  
                    <% 
                    PostDao postd=new PostDao(ConnectionProvider.getConnection());
                   ArrayList<Category> list= postd.getAllCategories();
                    for(Category c:list){        
                    %>
                   
                    <option value="<%= c.getCid() %>"><%= c.getName()%></option>
                    
                    <% 
                      }
                    
                    
                    
                    %>        
                </select>
              </div>
                
                
              <div class="form-group" ><!-- comment -->
                  <input name="pTitle" type="text" placeholder="Enter post Title" class="form-control"/>
              </div>
              <div class="form-group">
                  <textarea name="pContent" class="form-control" style="height: 200px" placeholder="Enter your content"></textarea>    
              </div>
              <div class="form-group">
                  <textarea name="pCode" class="form-control" style="height: 200px" placeholder="Enter your Program(if any)"></textarea>    
              </div>
              <div class="form-group">
                  <label>Select your pic</label>
                  <br><!-- comment -->
                  <input type="file" placeholder="Enter your pic.." name="pic"><!-- comment -->
                  
              </div>
                <div class="container text-center"><!-- comment -->
                    <button type="submit" class="btn btn-outline-primary">Post</button>
                </div>
          </form>     
      </div>
    </div>
  </div>
</div>
       
       
       
    </body>
    <!--javascript-->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!--jQuary-->
<script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
<script src="js/myjs.js" type="text/javascript"></script>

<script>
    $(document).ready(function(){
        let editStatus=false;
        $('#edit_profile_button').click(function(){
            if(editStatus == false){
                $('#profile_details').hide(); 
                $('#profile_edit').show();
                editStatus=true;
                $(this).text("Back");
            }else{
                $('#profile_details').show(); 
                $('#profile_edit').hide();
                editStatus=false;
                $(this).text("Edit");
            }
            
        });
    });
</script>
<!--now add post js-->
<script>
           $(document).ready(function (e) {
                //
                $("#add-post-form").on("submit", function (event) {
                    //this code gets called when form is submitted....
                    event.preventDefault();
                    console.log("you have clicked on submit..");
                    let form = new FormData(this);

                    //now requesting to server
                    $.ajax({
                        url: "AddPostServlet",
                        type: 'POST',
                        data: form,
                        success: function (data, textStatus, jqXHR) {
                            //success ..
                            console.log(data);
//                            if (data.trim() == 'done')
//                            {
//                                swal("Good job!", "saved successfully", "success");
//                            } else
//                            {
//                                swal("Error!!", "Something went wrong try again...", "error");
//                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            //error..
//                            swal("Error!!", "Something went wrong try again...", "error");
                        },
                        processData: false,
                        contentType: false
                    })
                })
            })   
</script>
</html>
