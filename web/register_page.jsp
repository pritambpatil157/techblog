<%-- 
    Document   : register_page.jsp
    Created on : 29-Jul-2023, 11:29:13 pm
    Author     : Pritam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!--css-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"><!-- comment -->
        <link href="css/mycss.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!-- comment -->
        <style>
            .banner-background{
               clip-path: polygon(30% 0%, 70% 0%, 100% 0, 100% 92%, 73% 100%, 34% 94%, 0 100%, 0 0);
            }
        </style>
    </head>
    <body>
        
         <!--nevbar-->
        <%@include file="normal_nevbar.jsp" %>
        
        
        <main class="primary-background p-5 banner-background" style="padding-bottom: 100px ">
            <div class="container" style="padding-bottom: 100px ">
                
                <div class="col-md-6 offset-md-3">
                    
                    <div class="card">
                        
                        <div class="card-header text-center text-white primary-background">
                            <span class="fa fa-3x  fa-user-circle"></span>
                            <p>Register here</p>
                            
                        </div>
                        <div class="card-body">
                            <form action="RegisterServlet" method="POST" id="reg-form">
                               <div class="form-group">
                                  <label for="user_name1">User Name</label>
                                  <input name="user_name" type="text" class="form-control" id="user_name" aria-describedby="emailHelp" placeholder="Enter name">
                                  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                               </div>
                              
                              
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input name="user_email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>


                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input name="user_password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                </div>
                              
                                <div class="form-group">
                                    <label for="gender">Select Gender</label>
                                    <br>
                                    <input type="radio" id="gender1" name="gender" value="Male">Male
                                    <input type="radio" id="gender1" name="gender" value="Female">Female
                                </div>
                                
                              <div class="form-group">
                                  
                                  <textarea id="about" class="form-control" name="user_about" rows="5"  placeholder="Enter Something About Your self"></textarea>
                                  
                                  
                              </div>
                              
                                         
                              <div class="form-check">
                                    <input name="check" type="checkbox" class="form-check-input" id="exampleCheck1">
                               <label class="form-check-label" for="exampleCheck1">Agree terms and conditions</label>
                               </div>
                              <br>
                              
                              <div class="container text-center" id="loader" style="display:none;">
                                 <span class="fa fa-refresh fa-spin fa-4x"></span>
                                 <h4>Please Wait..</h4>
                              </div>
                              
                              
                              <button id="submit-btn" type="submit" class="btn btn-primary">Submit</button>
                           </form>  
                            
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </main>
        
        
        
        
        
        
    </body>
<!--javascript-->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!--jQuary-->
<script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
<script src="js/myjs.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

<script>
            $(document).ready(function () {
                console.log("page is ready..........");
                $("#reg-form").on('submit',function(event){
                    event.preventDefault();
                    
                    
                    
                    let f=new FormData(this);
                    console.log(f);
                    $("#submit-btn").hide();
                    $("#loder").show();
                    
                    $.ajax({
                        url:"RegisterServlet",
                        data:f,
                        type:'Post',
                        success:function(data,textStatus,jqXHR){
                            console.log(data);
                            
                            $("#submit-btn").show();
                            $("#loder").hide();
                            
                            if(data.trim=="done"){
                                
                                swal("register successfully ..we redirecting the login page")
                                .then((value) => {
                                   window.location="login_page.jsp";
                                });
                             
                                
                            }else{
                                swal(data);
                            }
                            
                            
                           
                            
                            console.log("success....");                        
                        },
                        error:function (jqXHR,textStatus,errorThrown){
                             //console.log(jqXHR);
                            swal("Something went to wrong");
                            //console.log("error.....");   
                            
                        },
                        processData:false,
                        contentType:false
                    });
                });
            });
            
</script>

</html>
